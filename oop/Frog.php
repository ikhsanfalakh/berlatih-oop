<?php
	require_once('animal.php');

	class Frog extends Animal
	{
		public $legs = 4;
		public $lompat;

		public function jump() {
			$lompat = "hop hop";

			return $lompat;
		}
	}
?>