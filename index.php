<?php
	require_once('oop/Animal.php');
	require_once('oop/Ape.php');
	require_once('oop/Frog.php');

	$sheep = new Animal("shaun");

	echo "<h3>RELEASE 0</h3>";
	echo "<h4>Dengan Variable</h4>";

	echo "Nama hewan: ".$sheep->name."<br>"; // "shaun"
	echo "Jumlah kaki: ".$sheep->legs."<br>"; // 2
	echo "Berdarah dingin: ".$sheep->cold_blooded."<br>"; // false

	echo "<h4>Dengan Methode</h4>";
	
	echo "Nama hewan: ".$sheep->get_name()."<br>"; // "shaun"
	echo "Jumlah kaki: ".$sheep->get_legs()."<br>"; // 2
	echo "Berdarah dingin: ".$sheep->get_cold_blooded()."<br>"; // false

	echo "<h3>RELEASE 1</h3>";
	echo "<h4>Class Ape</h4>";

	$sungokong = new Ape("kera sakti");
	echo "Nama Hewan: ".$sungokong->name."<br>";// "kera sakti"
	echo "Jumlah kaki: ".$sungokong->legs."<br>"; // 2
	echo "Berdarah dingin: ".$sungokong->cold_blooded."<br>"; // false
	echo "Yel-yel: ".$sungokong->yell(); // "Auooo"
	
	echo "<h4>Class Frog</h4>";

	$kodok = new Frog("buduk");
	echo "Nama Hewan: ".$kodok->name."<br>";// "buduk"
	echo "Jumlah kaki: ".$kodok->legs."<br>"; // 2
	echo "Berdarah dingin: ".$kodok->cold_blooded."<br>"; // false
	echo "Melompat: ".$kodok->jump()."<br>"; // "hop hop"

?>